# qssi-user 12 SKQ1.211019.001 V13.0.4.0.SJGIDXM release-keys
- manufacturer: xiaomi
- platform: sm6150
- codename: surya
- flavor: qssi-user
- release: 12
- id: SKQ1.211019.001
- incremental: V13.0.4.0.SJGIDXM
- tags: release-keys
- fingerprint: POCO/surya_id/surya:12/RKQ1.211019.001/V13.0.4.0.SJGIDXM:user/release-keys
POCO/karna_id/karna:12/RKQ1.211019.001/V13.0.4.0.SJGIDXM:user/release-keys
POCO/surya_id/surya:12/RKQ1.211019.001/V13.0.4.0.SJGIDXM:user/release-keys
- is_ab: false
- brand: POCO
- branch: qssi-user-12-SKQ1.211019.001-V13.0.4.0.SJGIDXM-release-keys
- repo: poco_surya_dump
